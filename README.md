# Récupérer localement les dossiers de configuration
Récupérer sur la machine hôte les 3 dossiers "config", "logs" et "data".
## Configurer Gitlab
Éditer le fichier "config/gitab.rb" au besoin. Par défaut, il n'est pas nécessaire de l'éditer.
Le paragraphe "Configuration de Gitlab" du tutoriel libre "[Installation de Gitlab et Mattermost](http://framacloud.org/cultiver-son-jardin/installation-de-gitlab-et-mattermost/ "Configuration de Gitlab")" peut vous aider...

# Mettre à jour les ports et les chemins des volumes
Éditer le fichier docker-compose.yml et y mettre à jour les ports et les volumes utilisés.
Les volumes utilisés par la machine hôte doivent pointer sur les 3 dossiers que vous avez récupéré ci-dessus.
## Rappel
C'est la première des 2 valeurs qui correspond à celle de la machine hôte.
La seconde valeur (après les deux-points ':') correspond à la valeur utilisée par le container.

# Éxécuter l'image
docker-compose up -d

# Mettre à jour l'image
docker-compose pull

# Arrêter le service
docker-compose stop

# Utiliser le service après l'avoir lancé
[Page d'accueil Gitlab locale](http://localhost:8080)
*Bien sûr, le nom de la machine hôte ainsi que le port sont à mettre à jour en fonction des valeurs que vous aurez éventuellement surchargé dans le fichier docker-compose.yml*

# Aller plus loin
[Docker compose](https://docs.docker.com/compose)
